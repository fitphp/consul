# consul
### 为PHP更好的使用Consul而生

## 下载源码

直接使用：

```
git clone https://gitee.com/fitphp/consul.git
```

## 通过 composer 来安装

在你的 composer 项目中的 composer.json 文件中，添加这部分：

```json
{
    "require": {
        "fitphp/consul": "dev-master"
    }
}
```
    安装 composer require fitphp/consul

#### 通过Consul的HTTP接口操作，结合GuzzleHTTP

#### 启动Consul
consul agent -server -ui -data-dir=/data/consul -config-dir=/data/consul/config -client=0.0.0.0 -bootstrap -advertise=127.0.0.1 -node=xxx -dc=sh-1 > /dev/null &


```php
<?php
define('BASE_PATH', dirname(__DIR__));
include BASE_PATH . '/src/ServiceFactory.php';
if (file_exists(BASE_PATH . "/vendor/autoload.php")) {
    include BASE_PATH . "/vendor/autoload.php";
}

$data = array(
    'id' => 'service.id',//服务id
    'name' => 'service.name',//服务名
    'tags' => [1.0, master],//服务的tag，自定义，可以根据这个tag来区分同一个服务名的服务
    'address' => 'http://127.0.0.1',//服务注册到consul的IP，服务发现，发现的就是这个IP
    'port' => 8100,
    'enabletagoverride' => false,
    'check' => [
        'deregisterCriticalServiceAfter' => '90m',
         'interval' => '10s', //健康检查间隔时间，每隔10s，调用一次上面的URL
         'timeout'  => '1s',
         'http' => 'http://127.0.0.1:8100'
    ]       
);

// 服务名称
$serviceName = 'consul.service.name';

// 通过catalog注册
$sf =  FitPHP\Consul\ServiceFactory::getInstance();
$catalog = $sf->useService('catalog');
// 注册服务
var_dump($catalog->registerService($data));

        
//根据服务名获取地址信息
var_dump($catalog->service($serviceName));


//根据服务名获取健康地址信息
$health = $sf->useService('health');
var_dump($health->service($serviceName)); 


$kv = $sf->useService('kv');
$data = array('time' => time(), 'dns' => '127.0.0.1');
// 增加
var_dump($kv->put('testKeyValue', $data));
// 查询
var_dump($kv->get('testKeyValue'));

```