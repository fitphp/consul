<?php
/**
 * Agent.php Agent控制
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:34
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */
namespace FitPHP\Consul\Services;

use FitPHP\Consul\OptionsResolver;
use FitPHP\Consul\ServiceAbstract;

final class Agent extends ServiceAbstract implements AgentInterface
{
    /**
     * 返回本地agent注册的所有检查(包括配置文件和HTTP接口)
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function checks()
    {
        return $this->client->get('/v1/agent/checks');
    }

    /**
     * 返回本地agent注册的所有 服务
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function services()
    {
        return $this->client->get('/v1/agent/services');
    }

    /**
     * 返回agent在集群的gossip pool中看到的成员
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function members(array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('wan')),
        );

        return $this->client->get('/v1/agent/members', $params);
    }

    /**
     * 返回本地agent的配置和成员信息
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function self()
    {
        return $this->client->get('/v1/agent/self');
    }

    /**
     * 触发本地agent加入node
     * @param $address
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function join($address, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('wan')),
        );

        return $this->client->get('/v1/agent/join/'.$address, $params);
    }

    /**
     * 强制删除node
     * @param $node
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function forceLeave($node)
    {
        return $this->client->get('/v1/agent/force-leave/'.$node);
    }

    /**
     * 在本地agent增加一个检查项，使用PUT方法传输一个json格式的数据
     * @param $check
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function registerCheck($check)
    {
        $params = array(
            'body' => $check,
        );

        return $this->client->put('/v1/agent/check/register', $params);
    }

    /**
     * 注销一个本地agent的检查项
     * @param $checkId
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function deregisterCheck($checkId)
    {
        return $this->client->put('/v1/agent/check/deregister/'.$checkId);
    }

    /**
     * 设置一个本地检查项的状态为passing
     * @param $checkId
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function passCheck($checkId, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('note')),
        );

        return $this->client->put('/v1/agent/check/pass/'.$checkId, $params);
    }

    /**
     * 设置一个本地检查项的状态为warning
     * @param $checkId
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function warnCheck($checkId, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('note')),
        );

        return $this->client->put('/v1/agent/check/warn/'.$checkId, $params);
    }

    /**
     * 设置一个本地检查项的状态为critical
     * @param $checkId
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function failCheck($checkId, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('note')),
        );

        return $this->client->put('/v1/agent/check/fail/'.$checkId, $params);
    }

    /**
     * 在本地agent增加一个新的服务项，使用PUT方法传输一个json格式的数据
     * @param $service
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function registerService($service)
    {
        $params = array(
            'body' => $service,
        );

        return $this->client->put('/v1/agent/service/register', $params);
    }

    /**
     * 注销一个本地agent的服务项
     * @param $serviceId
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function deregisterService($serviceId)
    {
        return $this->client->put('/v1/agent/service/deregister/'.$serviceId);
    }
}
