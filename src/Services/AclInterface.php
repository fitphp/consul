<?php
/**
 * AclInterface.php ACL创建和管理
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:34
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul\Services;


interface AclInterface
{
    const SERVICE_NAME = 'acl';

    // Creates a new token with policy
    public function create();

    // Update the policy of a token
    public function update();

    // Destroys a given token
    public function destroy($id);

    // Queries the policy of a given token
    public function info($id);

    // Creates a new token by cloning an existing token
    public function cloneId($id);

    // Lists all the active tokens
    public function all();
}