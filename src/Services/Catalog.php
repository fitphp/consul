<?php
/**
 * Catalog.php 管理nodes和services
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:34
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul\Services;

use FitPHP\Consul\OptionsResolver;
use FitPHP\Consul\ServiceAbstract;

final class Catalog extends ServiceAbstract implements CatalogInterface
{
    /**
     * 注册一个新节点、服务或检查
     * @param $node
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function register($node)
    {
        $params = array(
            'body' => (string) $node,
        );

        return $this->client->get('/v1/catalog/register', $params);
    }

    /**
     * 注销节点、服务或检查
     * @param $node
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function deregister($node)
    {
        $params = array(
            'body' => (string) $node,
        );

        return $this->client->get('/v1/catalog/deregister', $params);
    }

    /**
     * 已知的数据中心名单
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function datacenters()
    {
        return $this->client->get('/v1/catalog/datacenters');
    }

    /**
     * 列出给定DC中的节点
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function nodes(array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->get('/v1/catalog/nodes', $params);
    }

    /**
     * 列出节点所提供的服务
     * @param $node
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function node($node, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->get('/v1/catalog/node/'.$node, $params);
    }

    /**
     * 列出给定DC中的服务
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function services(array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->get('/v1/catalog/services', $params);
    }

    /**
     * 列出给定服务中的节点
     * @param $service
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function service($service, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc', 'tag')),
        );

        return $this->client->get('/v1/catalog/service/'.$service, $params);
    }
}
