<?php
/**
 * Health.php 管理健康监测
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul\Services;

use FitPHP\Consul\OptionsResolver;
use FitPHP\Consul\ServiceAbstract;

final class Health extends ServiceAbstract implements HealthInterface
{
    /**
     * 返回node所定义的检查，可用参数?dc=
     * @param $node
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function node($node, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->get('/v1/health/node/'.$node, $params);
    }

    /**
     * 返回和服务相关联的检查，可用参数?dc=
     * @param $service
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function checks($service, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->get('/v1/health/checks/'.$service, $params);
    }

    /**
     * 返回给定datacenter中给定node中service
     * @param $service
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function service($service, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc', 'tag', 'passing')),
        );

        return $this->client->get('/v1/health/service/'.$service, $params);
    }

    /**
     * 返回给定datacenter中指定状态的服务，state可以是"any", "unknown", "passing", "warning", or "critical"，可用参数?dc=
     * @param $state
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function state($state, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->get('/v1/health/state/'.$state, $params);
    }
}
