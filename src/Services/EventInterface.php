<?php
/**
 * EventInterface.php 用户Events
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul\Services;


interface EventInterface
{
    const SERVICE_NAME = 'event';

    // 触发一个新的event，用户event需要name和其他可选的参数，使用PUT方法
    public function fire($name);

    // 返回agent知道的events
    public function all();
}