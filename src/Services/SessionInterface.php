<?php
/**
 * SessionInterface.php Session操作
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul\Services;

interface SessionInterface
{
    const SERVICE_NAME = 'session';

    public function create($body = null, array $options = array());

    public function destroy($sessionId, array $options = array());

    public function info($sessionId, array $options = array());

    public function node($node, array $options = array());

    public function all(array $options = array());

    public function renew($sessionId, array $options = array());
}
