<?php
/**
 * KV.php Key/Value存储
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul\Services;

use FitPHP\Consul\OptionsResolver;
use FitPHP\Consul\ServiceAbstract;

final class KV extends ServiceAbstract implements KVInterface
{
    public function get($key, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc', 'recurse', 'keys', 'separator', 'raw')),
        );

        return $this->client->get('v1/kv/'.ltrim($key, '/'), $params);
    }

    public function put($key, $value, array $options = array())
    {
        $params = array(
            'body' => $value,
            'query' => OptionsResolver::resolve($options, array('dc', 'flags', 'cas', 'acquire', 'release')),
        );

        return $this->client->put('/v1/kv/'.ltrim($key, '/'), $params);
    }

    public function delete($key, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc', 'recurse')),
        );

        return $this->client->delete('/v1/kv/'.ltrim($key, '/'), $params);
    }
}
