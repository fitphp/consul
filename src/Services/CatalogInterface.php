<?php
/**
 * CatalogInterface.php 管理nodes和services
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:34
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul\Services;


interface CatalogInterface
{
    const SERVICE_NAME = 'catalog';

    public function register($node);

    public function deregister($node);

    public function datacenters();

    public function nodes(array $options = array());

    public function node($node, array $options = array());

    public function services(array $options = array());

    public function service($service, array $options = array());
}
