<?php
/**
 * Session.php Session操作
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */
namespace FitPHP\Consul\Services;

use FitPHP\Consul\OptionsResolver;
use FitPHP\Consul\ServiceAbstract;

final class Session extends ServiceAbstract implements SessionInterface
{
    /**
     * Creates a new session
     * @param null $body
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function create($body = null, array $options = array())
    {
        $params = array(
            'body' => $body,
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->put('/v1/session/create', $params);
    }

    /**
     * Destroys a given session
     * @param $sessionId
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function destroy($sessionId, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->put('/v1/session/destroy/'.$sessionId, $params);
    }

    /**
     * Queries a given session
     * @param $sessionId
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function info($sessionId, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->get('/v1/session/info/'.$sessionId, $params);
    }

    /**
     * Lists sessions belonging to a node
     * @param $node
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function node($node, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->get('/v1/session/node/'.$node, $params);
    }

    /**
     * Lists all the active sessions
     * @param array $options
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function all(array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->get('/v1/session/list', $params);
    }

    public function renew($sessionId, array $options = array())
    {
        $params = array(
            'query' => OptionsResolver::resolve($options, array('dc')),
        );

        return $this->client->put('/v1/session/renew/'.$sessionId, $params);
    }
}
