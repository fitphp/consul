<?php
/**
 * ServiceAbstract.php ACL创建和管理
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:41
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul\Services;

use FitPHP\Consul\ServiceAbstract;

final class Acl extends ServiceAbstract  implements AclInterface
{
    /**
     * Creates a new token with policy
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function create()
    {
        return $this->client->get('/v1/acl/create');
    }

    /**
     * Update the policy of a token
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function update()
    {
        return $this->client->get('/v1/acl/update');
    }

    /**
     * Destroys a given token
     * @param $id
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function destroy($id)
    {
        return $this->client->get('/v1/acl/destroy/'.$id);
    }

    /**
     * Queries the policy of a given token
     * @param $id
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function info($id)
    {
        return $this->client->get('/v1/acl/info/'.$id);
    }

    /**
     * Creates a new token by cloning an existing token
     * @param $id
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function cloneId($id)
    {
        return $this->client->get('/v1/acl/clone/'.$id);
    }

    /**
     * Lists all the active tokens
     * @return \FitPHP\Consul\ConsulResponse
     */
    public function all()
    {
        return $this->client->get('/v1/acl/list');
    }
}