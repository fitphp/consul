<?php
/**
 * HealthInterface.php 管理健康监测
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul\Services;


interface HealthInterface
{
    const SERVICE_NAME = 'health';

    public function node($node, array $options = array());

    public function checks($service, array $options = array());

    public function service($service, array $options = array());

    public function state($state, array $options = array());
}
