<?php
/**
 * StatusInterface.php Consul系统状态
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul\Services;


interface StatusInterface
{
    const SERVICE_NAME = 'status';

    // 返回当前集群的Raft leader
    public function leader();

    // 返回当前集群中同事
    public function peers();
}