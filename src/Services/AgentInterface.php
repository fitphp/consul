<?php
/**
 * AgentInterface.php Agent控制
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:34
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul\Services;


interface AgentInterface
{
    const SERVICE_NAME = 'agent';

    public function checks();

    public function services();

    public function members(array $options = array());

    public function self();

    public function join($address, array $options = array());

    public function forceLeave($node);

    public function registerCheck($check);

    public function deregisterCheck($checkId);

    public function passCheck($checkId, array $options = array());

    public function warnCheck($checkId, array $options = array());

    public function failCheck($checkId, array $options = array());

    public function registerService($service);

    public function deregisterService($serviceId);
}
