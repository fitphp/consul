<?php
/**
 * ClientInterface.php
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

namespace FitPHP\Consul;


interface ClientInterface
{
    public function get($url = null, array $options = array());

    public function head($url, array $options = array());

    public function delete($url, array $options = array());

    public function put($url, array $options = array());

    public function patch($url, array $options = array());

    public function post($url, array $options = array());

    public function options($url, array $options = array());
}
