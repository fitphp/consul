<?php
/**
 * OptionsResolverTest.php
 *
 * @package fitphp.com
 * @version {$Id$}
 * @copyright 2009-2020 Shanghai Reepu Information Tech. Co., Ltd.
 * @license MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */
namespace FitPHP\Consul\Tests;

use FitPHP\Consul\OptionsResolver;

class OptionsResolverTest extends \PHPUnit_Framework_TestCase
{
    public function testResolve()
    {
        $options = array(
            'foo' => 'bar',
            'hello' => 'world',
            'baz' => 'inga',
        );

        $availableOptions = array(
            'foo', 'baz',
        );

        $result = OptionsResolver::resolve($options, $availableOptions);

        $expected = array(
            'foo' => 'bar',
            'baz' => 'inga',
        );

        $this->assertSame($expected, $result);
    }

    public function testResolveWithoutMatchingOptions()
    {
        $options = array(
            'hello' => 'world',
        );

        $availableOptions = array(
            'foo', 'baz',
        );

        $result = OptionsResolver::resolve($options, $availableOptions);

        $this->assertSame(array(), $result);
    }
}
