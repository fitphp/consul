<?php
/**
 * ConsulExceptionInterface.php
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean since.
 * @datetime 2018/2/5 16:55
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */
namespace FitPHP\Consul\Exception;

interface ConsulExceptionInterface
{
}
