<?php
/**
 * ConsulResponse.php
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */
namespace FitPHP\Consul;

final class ConsulResponse
{
    private $headers;
    private $body;
    private $status;

    public function __construct($headers, $body, $status = 200)
    {
        $this->headers = $headers;
        $this->body = $body;
        $this->status = $status;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getBody()
    {
        return $this->getArray();
    }

    public function getStatusCode()
    {
        return $this->status;
    }

    public function getJson()
    {
        return $this->body;
    }

    public function getArray()
    {
        return json_decode($this->body, true);
    }
}
