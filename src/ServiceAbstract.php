<?php
/**
 * ServiceAbstract.php
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */
namespace FitPHP\Consul;

abstract class ServiceAbstract
{
    protected $client;

    public function __construct(Client $client = null)
    {
        $this->client = $client ?: new Client();
    }
}