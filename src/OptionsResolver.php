<?php
/**
 * OptionsResolver.php
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean <hongbin.hsu@qq.com> since.
 * @datetime 2017/12/3 下午8:40
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */
namespace FitPHP\Consul;

final class OptionsResolver
{
    public static function resolve(array $options, array $availableOptions)
    {
        return array_intersect_key($options, array_flip($availableOptions));
    }
}
