<?php
/**
 * health
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @Author: Boolean since.
 * @DateTime: 2020-04-14 09:51
 * =================================================================
 * 版权所有 (C) 2009-2020 Reepu.com，并保留所有权利。
 * 网站地址:http://www.reepu.com/
 * =================================================================
 */

define('BASE_PATH', dirname(__DIR__));
include BASE_PATH . '/src/ServiceFactory.php';
if (file_exists(BASE_PATH . "/vendor/autoload.php")) {
    include BASE_PATH . "/vendor/autoload.php";
}

$sf =  FitPHP\Consul\ServiceFactory::getInstance();
$health = $sf->useService('health');

//根据服务名获取地址信息
$serviceName = 'consul.service.name';
var_dump($health->service($serviceName)->getJson());
var_dump($health->service($serviceName)->getArray());
var_dump($health->service($serviceName)->getStatusCode());