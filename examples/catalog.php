<?php
/**
 * catalog.php 服务发现
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean since.
 * @datetime 2018/2/6 下午11:16
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

define('BASE_PATH', dirname(__DIR__));
include BASE_PATH . '/src/ServiceFactory.php';
if (file_exists(BASE_PATH . "/vendor/autoload.php")) {
    include BASE_PATH . "/vendor/autoload.php";
}

$sf =  FitPHP\Consul\ServiceFactory::getInstance();
$catalog = $sf->useService('catalog');

//根据服务名获取地址信息
$serviceName = 'consul.service.name';
var_dump($catalog->service($serviceName)->getJson());
var_dump($catalog->service($serviceName)->getArray());
var_dump($catalog->service($serviceName)->getStatusCode());