<?php
/**
 * kv.php KV操作
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean since.
 * @datetime 2018/2/5 16:55
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */
define('BASE_PATH', dirname(__DIR__));
include BASE_PATH . '/src/ServiceFactory.php';
if (file_exists(BASE_PATH . "/vendor/autoload.php")) {
    include BASE_PATH . "/vendor/autoload.php";
}

$sf =  FitPHP\Consul\ServiceFactory::getInstance();
$kv = $sf->useService('kv');
$data = array('time' => time(), 'dns' => '127.0.0.1');
// 增加
var_dump($kv->put('testKeyValue', $data));
// 查询
var_dump($kv->get('testKeyValue')->getJson());
var_dump($kv->get('testKeyValue')->getArray());
var_dump($kv->get('testKeyValue')->getStatusCode());