<?php
/**
 * service.php 服务注册
 *
 * @package fitphp.com
 * @version {$Id$}
 * @Copyright 2009-2020 FitPHP Item.
 * @License MIT
 * @author Boolean since.
 * @datetime 2018/2/5 16:55
 * =================================================================
 * 版权所有 (C) 2009-2020 fitphp.com，并保留所有权利。
 * 网站地址:http://www.fitphp.com/
 */

define('BASE_PATH', dirname(__DIR__));
include BASE_PATH . '/src/ServiceFactory.php';
if (file_exists(BASE_PATH . "/vendor/autoload.php")) {
    include BASE_PATH . "/vendor/autoload.php";
}

$sf =  FitPHP\Consul\ServiceFactory::getInstance();
$agent = $sf->useService('agent');

$data = array(
    'id' => 'consul.service.id',//服务id
    'name' => 'consul.service.name',//服务名
    'tags' => array('primary', 'v1'),//服务的tag，自定义，可以根据这个tag来区分同一个服务名的服务
    'address' => '127.0.0.1',//服务注册到consul的IP，服务发现，发现的就是这个IP
    'port' => 8081,
    'enabletagoverride' => false,
    'check' => array(
        'deregistercriticalserviceafter' => '90m',
        'http' => 'http://www.baidu.com', //指定健康检查的URL，调用后只要返回20X，consul都认为是健康的
        'interval' => '10s' //健康检查间隔时间，每隔10s，调用一次上面的URL
    ), //健康检查部分

);

// 注册服务
var_dump($agent->registerService($data));